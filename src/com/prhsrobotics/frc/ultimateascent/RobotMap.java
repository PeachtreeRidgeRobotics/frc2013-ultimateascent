package com.prhsrobotics.frc.ultimateascent;

import com.prhsrobotics.frc.ultimateascent.*;
import edu.wpi.first.wpilibj.*; 
/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
    
    // drive motors
    public static final int frontLeftDriveMotor = 1;
    public static final int rearLeftDriveMotor = 2;
    public static final int frontRightDriveMotor = 3;
    public static final int rearRightDriveMotor = 4;

    // shooter motors
    public static final int[] shooterMotor = {5, 6};
    
    // shooter limit switches
    public static final int shooterLowLimitSwitch = 1;
    public static final int shooterHighLimitSwitch = 2;
    
    // elevator relay
    public static final int elevatorRelay = 1;
    
    // launcher relay
    public static final int launcherRelay = 2;
    
    // elevator potentiometer
    public static final int elevatorPotentiometer = 2;
    
    // joysticks
    public static final int driverJoystick = 1;
    public static final int manipulatorJoystick = 2;
}
