package com.prhsrobotics.frc.ultimateascent.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.*;
import com.prhsrobotics.frc.ultimateascent.RobotMap;
import com.prhsrobotics.frc.ultimateascent.commands.ArcadeDrive;

/**
 * Controls for the chassis subsystem of the robot.
 * @author Nitant
 */
public class Chassis extends Subsystem {
    Talon frontLeftDriveMotor = new Talon(RobotMap.frontLeftDriveMotor);
    Talon rearLeftDriveMotor = new Talon(RobotMap.rearLeftDriveMotor);
    Talon frontRightDriveMotor = new Talon(RobotMap.frontRightDriveMotor);
    Talon rearRightDriveMotor = new Talon(RobotMap.rearRightDriveMotor);
        
    public RobotDrive drivetrain = new RobotDrive(frontLeftDriveMotor, rearLeftDriveMotor, frontRightDriveMotor, rearRightDriveMotor);
    
    public Chassis(String name) {
        super(name);
        
        drivetrain.setInvertedMotor(RobotDrive.MotorType.kFrontLeft, true);
        drivetrain.setInvertedMotor(RobotDrive.MotorType.kFrontRight, true);
        drivetrain.setInvertedMotor(RobotDrive.MotorType.kRearLeft, true);
        drivetrain.setInvertedMotor(RobotDrive.MotorType.kRearRight, true);
    }
    
    public Chassis() {
        super();
        
        drivetrain.setInvertedMotor(RobotDrive.MotorType.kFrontLeft, true);
        drivetrain.setInvertedMotor(RobotDrive.MotorType.kFrontRight, true);
        drivetrain.setInvertedMotor(RobotDrive.MotorType.kRearLeft, true);
        drivetrain.setInvertedMotor(RobotDrive.MotorType.kRearRight, true);
    }
    
    /**
     * Drives the robot given a power and direction value.
     * @param power what power the robot should move with
     * @param direction what direction the robot should move in (positive right, negative left)
     */
    public void arcadeDrive(double power, double direction) {
        drivetrain.arcadeDrive(power, direction);
    }
    
    /**
     * Moves the robot straight forward or backward.
     * @param power what power the robot should move with
     */
    public void driveStraight(double power) {
        drivetrain.drive(power, 0);
    }
    
    /**
     * Turns the motor left or right.
     * @param direction which direction the robot should move in (positive right, negative left)
     */
    public void turn(double direction) {
        drivetrain.drive(1, direction);
    }
    
    /**
     * Stops the robot.
     */
    public void brake() {
        drivetrain.drive(0, 0);
    }

    /**
     * Specifies no default command for the system.
     */
    protected void initDefaultCommand() {
        setDefaultCommand(new ArcadeDrive());
    }
}