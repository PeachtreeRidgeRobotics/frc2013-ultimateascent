/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prhsrobotics.frc.ultimateascent.commands;

/**
 *
 * @author Nitant
 */
public class ArcadeDrive extends CommandBase {
    
    public ArcadeDrive() {
        requires(chassis);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
        chassis.drivetrain.arcadeDrive(oi.driverJoystick, true);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return false;
    }

    // Called once after isFinished returns true
    protected void end() {
        chassis.brake();
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
        chassis.brake();
    }
}